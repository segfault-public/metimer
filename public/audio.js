let audioCtx;

if(window.webkitAudioContext) {
    audioCtx = new window.webkitAudioContext();
} else {
    audioCtx = new window.AudioContext();
}

function playbuffer(buffer) {
	const play = audioCtx.createBufferSource();
	play.buffer = buffer;
	play.connect(audioCtx.destination);
	play.start();
	return play;
}

function _interleave(input) {
    let buffer = input.getChannelData(0),
      length = buffer.length * 2,
      result = new Float32Array(length),
      index = 0,
      inputIndex = 0;

    while (index < length) {
      result[index++] = buffer[inputIndex];
      result[index++] = buffer[inputIndex];
      inputIndex++;
    }
    return result;
  }

function  _writeString(dataview, offset, header) {
    let output;
    for (var i = 0; i < header.length; i++) {
      dataview.setUint8(offset + i, header.charCodeAt(i));
    }
  }

function   _floatTo16BitPCM(dataview, buffer, offset) {
    for (var i = 0; i < buffer.length; i++, offset += 2) {
      let tmp = Math.max(-1, Math.min(1, buffer[i]));
      dataview.setInt16(offset, tmp < 0 ? tmp * 0x8000 : tmp * 0x7fff, true);
    }
    return dataview;
  }

function   _writeHeaders(buffer, sampleRate) {
    let arrayBuffer = new ArrayBuffer(44 + buffer.length * 2),
      view = new DataView(arrayBuffer);

    _writeString(view, 0, "RIFF");
    view.setUint32(4, 32 + buffer.length * 2, true);
    _writeString(view, 8, "WAVE");
    _writeString(view, 12, "fmt ");
    view.setUint32(16, 16, true);
    view.setUint16(20, 1, true);
    view.setUint16(22, 2, true);
    view.setUint32(24, sampleRate, true);
    view.setUint32(28, sampleRate * 4, true);
    view.setUint16(32, 4, true);
    view.setUint16(34, 16, true);
    _writeString(view, 36, "data");
    view.setUint32(40, buffer.length * 2, true);

    return _floatTo16BitPCM(view, buffer, 44);
  }


function download( blob, filename ) {
	var link = document.createElement('a');
	document.body.appendChild(link);
	link.style = "display:none";
	link.href = window.URL.createObjectURL(blob);
	link.download = filename;
	link.click();
	window.URL.revokeObjectURL(link.href);
}

function downloadWave(buffer) {
	const type = "audio/wav";
	const recorded = this._interleave(buffer);
	const sampleRate = buffer.sampleRate;
	const dataview = this._writeHeaders(recorded, sampleRate);
	const audioBlob = new Blob([dataview], { type: type });

	download(audioBlob, "test-audio.wav");	

}
function   _maxDuration(buffers) {
    return Math.max.apply(Math, buffers.map(buffer => buffer.duration));
  }

function merge(buffers) {
	let output = audioCtx.createBuffer(
		1,
		buffers[0].sampleRate * _maxDuration(buffers),
		buffers[0].sampleRate
		);
    buffers.map(buffer => {
      for (let i = buffer.getChannelData(0).length - 1; i >= 0; i--) {
        output.getChannelData(0)[i] += buffer.getChannelData(0)[i];
      }
    });
    return output;
}

function mergeWithOffset(destination, target, offset) {
    for (let i = target.getChannelData(0).length - 1; i >= 0; i--) {
        destination.getChannelData(0)[offset+i] += target.getChannelData(0)[i];
    }
    return destination;
}

function addOffset(buffer, offset) {
	offset = offset * buffer.sampleRate;
	let output = audioCtx.createBuffer(
		1, 
		buffer.length + offset,
		buffer.sampleRate);
	output.getChannelData(0).set(buffer.getChannelData(0), offset);
	return output;

}

let outAudio;
let cachedSongs = new Array();

function appendNextSong(index, sound_list, callback) {

	if(index == sound_list.length-1) {
		soundFinish(sound_list, callback);
	}else {
		loadIndex(index+1, callback, sound_list);
	}
	
}

function soundFinish(sound_list, callback) {

	let max_length = 0;
	for(var i = 0; i < sound_list.length; i++) {
		var data = cachedSongs.find(o => o.url = sound_list[i].url).data;
		var offset = sound_list[i].offset * data.sampleRate;

		var l = offset + data.length;
		max_length = Math.max(max_length, l);
	}
	let output = createEmptySound(max_length, 44100);
	for(var i = 0; i < sound_list.length; i++) {
		var offset = sound_list[i].offset * data.sampleRate;
		var data = cachedSongs.find(o => o.url = sound_list[i].url).data;
		output = mergeWithOffset(output, data, offset);
	}

	callback(output);
}

function getSound(index, callback, sound_list) {
  sound_url = sound_list[index].url;
  offset = sound_list[index].offset;

  let s = cachedSongs.find(o => o.url = sound_url);
  if(s) {
	console.log("use cache for: " + index + ": " + sound_url);
	  appendNextSong(index, sound_list, callback);
  } else {
	console.log("download: " + index + ": " + sound_url)
    request = new XMLHttpRequest();
    request.open('GET', sound_url, true);
    request.responseType = 'arraybuffer';
    var loaded = false;
    request.onload = function() {
      let audioData = request.response;
      //console.log(audioData);

      audioCtx.decodeAudioData(audioData, function(buffer) {

          cachedSongs.push({url: sound_url, data: buffer});
	  appendNextSong(index, sound_list, callback);

//          let sec = addOffset(buffer, offset);
//          dest_source = merge([dest_source, sec]);
//
//
//          if(index == sound_list.length-1) {
//          	callback(dest_source);
//          }else {
//          	loadIndex(dest_source, index+1, callback, sound_list);
//          }
        },

        function(e){"Error with decoding audio data" + e.error});

    }

    request.send();
  }
  
}

function createEmptySound(length, sampleRate) {
	return audioCtx.createBuffer(1, length, sampleRate);
}

let dest_sound;

let test_list = [
	{url: "bell.mp3", offset: 0},
	{url: "bell.mp3", offset: 1},
	{url: "bell.mp3", offset: 2},
	{url: "bell.mp3", offset: 3},
]

function loadIndex(index, callback, sound_list) {
	getSound(index, callback, sound_list);
}

function ready(dest_source) {
	playbuffer(dest_source);
	dest_sound = dest_source;
}

function createSound(callback, sound_list) {
	//dest_sound = createEmptySound(1, 44100);
	loadIndex(0, callback, sound_list);
}

//createSound(ready, test_list);
